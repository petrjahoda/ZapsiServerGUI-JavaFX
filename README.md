# Zapsi Server 3.0 GUI
GUI that manages service that downloads data from devices made by www.zapsi.eu.
Service is here https://github.com/jahaman/ZapsiServerService

## Features
* show "what is happening" log
* settings
  * database connection
  * email settings
* filtering
* device settings

www.zapsi.eu © 2017

![image 004](https://user-images.githubusercontent.com/11396401/32288121-364b9efe-bf33-11e7-8d19-0215cbafe15b.png)

