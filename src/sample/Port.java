package sample;

public class Port {

    private int OID;
    private String name;
    private int deviceID;
    private int portType;
    private int portNumber;
    private String settings;
    private String unit;

    public Port(int OID, String name, int deviceID, int portType, int portNumber, String settings, String unit) {
        this.OID = OID;
        this.name = name;
        this.deviceID = deviceID;
        this.portType = portType;
        this.settings = settings;
        this.unit = unit;
        this.portNumber = portNumber;
    }

    public int getOID() {
        return OID;
    }

    public void setOID(int OID) {
        this.OID = OID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(int deviceID) {
        this.deviceID = deviceID;
    }

    public int getPortType() {
        return portType;
    }

    public void setPortType(int portType) {
        this.portType = portType;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }
}
