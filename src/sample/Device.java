package sample;

public class Device {

    private String name;
    private int OID;
    private String ipAddress;
    private boolean actvated;


    public Device(int OID, String name, String ipAddress, boolean actvated) {
        this.name = name;
        this.OID = OID;
        this.ipAddress = ipAddress;
        this.actvated = actvated;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOID() {
        return OID;
    }

    public void setOID(int OID) {
        this.OID = OID;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isActvated() {
        return actvated;
    }

    public void setActvated(boolean actvated) {
        this.actvated = actvated;
    }
}
