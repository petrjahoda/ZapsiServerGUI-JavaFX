package sample;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Controller implements Initializable {
    public TextArea logArea;
    public CheckBox pauseButton;
    public TextField filterField;
    public TextField databaseAddress;
    public TextField databaseName;
    public TextField databaseUsername;
    public TextField databasePassword;
    public TextField customer;
    public TextField emailsFrom;
    public TextField emailsTo;
    public TextField smtpHost;
    public TextField smtpPassword;
    public TextField port;
    public Text serviceInstallationStatus;
    public Text serviceRunningStatus;
    public Button installServiceButton;
    public Button runServiceButton;
    public Button saveButton;
    public TextField deviceNameField;
    public TextField deviceIPField;
    public CheckBox deviceIsActiveCheckBox;
    public Button saveDeviceButton;
    public TextField portNameField;
    public TextField portUnitField;
    public TextField portSettingsField;
    public RadioButton portIsDigital;
    public RadioButton portIsAnalog;
    public TextField portNumberField;
    public Button savePortButton;
    public Button removeDeviceButton;
    public Button removePortButton;
    public TableColumn deviceIdColumn;
    public TableColumn deviceNameColumn;
    public TableColumn deviceIpColumn;
    public TableView<Device> devicesTable;
    public Text informationField;
    public TableColumn portIdColumn;
    public TableColumn portPortColumn;
    public TableColumn portNameColumn;
    public TableView portsTable;
    private boolean pauseLogCheckBoxIsSelected;
    private boolean serviceInstalled;
    private boolean serviceRunning;
    private ServiceStatus serviceStatus = ServiceStatus.NONE;
    private Connection mySQLConnection;
    private int selectedDeviceRow;
    private int selectedPortRow;
    private int selectedDeviceOID;
    ObservableList<Device> devicesFromDatabase = FXCollections.observableArrayList();
    ObservableList<Port> portsFromDatabase = FXCollections.observableArrayList();


    public enum ServiceStatus {
        RUNNING,
        STOPPED,
        NONE
    }

    public void removeDevice(ActionEvent actionEvent) {
        int deviceOID = getDeviceOID(selectedDeviceRow);
        try {
            String query = "DELETE FROM zapsi2.device where OID=" + deviceOID;
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot delete device");
            informationField.setText("Cannot delete device, that has ports in use.");
        }
        devicesFromDatabase.clear();
        refreshDevicesTable();
    }


    private int getDeviceOID(int selectedDeviceRow) {
        int deviceOID = 0;
        for (int positionInList = 0; positionInList < devicesFromDatabase.size(); positionInList++) {
            if (positionInList == selectedDeviceRow) {
                Device device = devicesFromDatabase.get(positionInList);
                deviceOID = device.getOID();
            }

        }
        return deviceOID;

    }


    private int getPortOID(int selectedDeviceRow) {
        int portOID = 0;
        for (int positionInList = 0; positionInList < portsFromDatabase.size(); positionInList++) {
            if (positionInList == selectedDeviceRow) {
                Port port = portsFromDatabase.get(positionInList);
                portOID = port.getOID();
            }

        }
        return portOID;

    }

    public void removePort(ActionEvent actionEvent) {
        int portOID = getPortOID(selectedPortRow);
        try {
            String query = "DELETE FROM zapsi2.device_port where OID=" + portOID;
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot delete device");
            informationField.setText("Cannot delete port, that has saved data.");
        }
        refreshPortsTable(selectedDeviceOID);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            createConfigFile();
            loadSettingsFromFile();
            initiateLogTab();
        } catch (IOException e) {
            e.printStackTrace();
        }
        checkForServiceStatus();
        openDatabaseConnection();
        refreshDevicesTable();
        initiateDevicesTableGUI();

        initiateListeners();

    }

    private void initiateListeners() {
        devicesTable.getSelectionModel().selectedItemProperty().addListener((ChangeListener) (observableValue, oldValue, newValue) -> {
            if (devicesTable.getSelectionModel().getSelectedItem() != null) {
                informationField.setText("");
                TableView.TableViewSelectionModel selectionModel = devicesTable.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
//                Object val = tablePosition.getTableColumn().getCellData(newValue);
//                int val = tablePosition.getRow();
                selectedDeviceRow = tablePosition.getRow();
                Device device = devicesFromDatabase.get(selectedDeviceRow);
                deviceNameField.setText(device.getName());
                deviceIPField.setText(device.getIpAddress());
                System.out.println(device.isActvated());
                if (device.isActvated()) {
                    deviceIsActiveCheckBox.setSelected(true);
                } else {
                    deviceIsActiveCheckBox.setSelected(false);
                }
                removeDeviceButton.setDisable(false);
                refreshPortsTable(device.getOID());
                selectedDeviceOID = device.getOID();
            } else {
                removeDeviceButton.setDisable(true);
            }
        });

        portsTable.getSelectionModel().selectedItemProperty().addListener((ChangeListener) (observableValue, oldValue, newValue) -> {
            if (portsTable.getSelectionModel().getSelectedItem() != null) {
                informationField.setText("");
                TableView.TableViewSelectionModel selectionModel = portsTable.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
//                Object val = tablePosition.getTableColumn().getCellData(newValue);
//                int val = tablePosition.getRow();
                selectedPortRow = tablePosition.getRow();
                Port port = portsFromDatabase.get(selectedPortRow);
                portNumberField.setText(String.valueOf(port.getPortNumber()));
                portNameField.setText(port.getName());
                portUnitField.setText(port.getUnit());
                portSettingsField.setText(port.getSettings());
                if (port.getPortType() == 1) {
                    portIsDigital.setSelected(true);
                    portIsAnalog.setSelected(false);
                } else {
                    portIsDigital.setSelected(false);
                    portIsAnalog.setSelected(true);
                }
                removePortButton.setDisable(false);

            } else {
                removePortButton.setDisable(true);
            }
        });


    }

    private void refreshPortsTable(int deviceOID) {
        System.out.println("DEVICE OID is " + deviceOID);
        portsFromDatabase.clear();
        portIdColumn.setCellValueFactory(new PropertyValueFactory<>("OID"));
        portNameColumn.setCellValueFactory(new PropertyValueFactory<>("Name"));
        portPortColumn.setCellValueFactory(new PropertyValueFactory<>("PortNumber"));
        try {
            String query = "SELECT * FROM zapsi2.device_port where deviceID=" + deviceOID;
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int oid = resultSet.getInt("OID");
                int portNumber = resultSet.getInt("PortNumber");
                int portType = resultSet.getInt("PortType");
                String name = resultSet.getString("Name");
                String unit = resultSet.getString("Unit");
                String settings = resultSet.getString("Setting");
                Port port = new Port(oid, name, deviceOID, portType, portNumber, settings, unit);
                portsFromDatabase.add(port);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        portsTable.setItems(portsFromDatabase);
    }

    private void openDatabaseConnection() {
        try {
            String databaseConnection = "jdbc:mysql://" + databaseAddress.getText() + ":3306/" + databaseName.getText() + "?autoReconnect=true&useSSL=false";
            mySQLConnection = DriverManager.getConnection(databaseConnection, databaseUsername.getText(), databasePassword.getText());
            System.out.println("Connected");

        } catch (SQLException cannotConnect) {
            System.out.println("cannot connect");
        }
    }

    private void initiateDevicesTableGUI() {
        deviceIdColumn.setStyle("-fx-alignment: CENTER;");
        deviceNameColumn.setStyle("-fx-alignment: CENTER;");
        deviceIpColumn.setStyle("-fx-alignment: CENTER;");
    }

    private void refreshDevicesTable() {
        deviceIdColumn.setCellValueFactory(new PropertyValueFactory<>("OID"));
        deviceNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        deviceIpColumn.setCellValueFactory(new PropertyValueFactory<>("ipAddress"));

        try {
            String query = "SELECT * FROM zapsi2.device where devicetype=1";
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int oid = resultSet.getInt("oid");
                String name = resultSet.getString("name");
                String ipAddress = resultSet.getString("ipAddress");
                boolean activated = resultSet.getBoolean("activated");
                Device device = new Device(oid, name, ipAddress, activated);
                devicesFromDatabase.add(device);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        devicesTable.setItems(devicesFromDatabase);
    }


    private void initiateLogTab() {
        logArea.clear();
        refreshLogAndInfoStatus();
        KeyFrame[] arrkeyFrame = new KeyFrame[1];
        arrkeyFrame[0] = new KeyFrame(Duration.seconds(1.0), event -> refreshLogAndInfoStatus());
        Timeline logRefresh = new Timeline(arrkeyFrame);
        logRefresh.setCycleCount(-1);
        logRefresh.play();
    }

    private void refreshLogAndInfoStatus() {
        try {
            String dataToDisplayInLogTextField = "";
            FileReader fr = new FileReader("log\\log.txt");
            BufferedReader br = new BufferedReader(fr);
            if (!pauseLogCheckBoxIsSelected) {
                updateLogViewFromFile(dataToDisplayInLogTextField);
            }
            fr.close();
            br.close();
        } catch (IOException e) {
            System.out.println("No log file present");
        }
    }

    private void updateLogViewFromFile(String dataToDisplayInLogTextField) {
        try {
            File file = new File("log\\log.txt");
            int linesToDisplay = 100;
            ReversedLinesFileReader object = new ReversedLinesFileReader(file, UTF_8);
            StringBuilder dataToDisplayInLogTextFieldBuilder = new StringBuilder(dataToDisplayInLogTextField);
            if (file.length() - 1 < 100) {
                linesToDisplay = (int) (file.length() - 1);
            }
            for (int i = 0; i < linesToDisplay; i++) {
                String line = object.readLine();
                if (line == null) {
                    break;
                }
                if (filterField.getText().length() > 0) {
                    if (line.contains(filterField.getText())) {
                        dataToDisplayInLogTextFieldBuilder.append(line).append("\n");
                    }
                } else {
                    dataToDisplayInLogTextFieldBuilder.append(line).append("\n");
                }
            }
            dataToDisplayInLogTextField = dataToDisplayInLogTextFieldBuilder.toString();
            logArea.setText(dataToDisplayInLogTextField);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadSettingsFromFile() throws IOException {
        String actualLine;
        FileReader fr = new FileReader("config.txt");
        BufferedReader br = new BufferedReader(fr);
        while ((actualLine = br.readLine()) != null) {
            if (actualLine.contains("[databaseAddress]")) {
                databaseAddress.setText(actualLine.substring(18));
            } else if (actualLine.contains("[databaseName]")) {
                databaseName.setText(actualLine.substring(15));
            } else if (actualLine.contains("[userName]")) {
                databaseUsername.setText(actualLine.substring(11));
            } else if (actualLine.contains("[userPassword]")) {
                databasePassword.setText(actualLine.substring(15));
            } else if (actualLine.contains("[customer]")) {
                customer.setText(actualLine.substring(11));
            } else if (actualLine.contains("[emailsFrom]")) {
                emailsFrom.setText(actualLine.substring(13));
            } else if (actualLine.contains("[port]")) {
                port.setText(actualLine.substring(7));
            } else if (actualLine.contains("[emailTo]")) {
                emailsTo.setText(actualLine.substring(10));
            } else if (actualLine.contains("[smtpHost]")) {
                smtpHost.setText(actualLine.substring(11));
            } else if (actualLine.contains("[smtpPassword]")) {
                smtpPassword.setText(actualLine.substring(15));
                System.out.println(actualLine.substring(15));
            }
        }
        br.close();
        fr.close();
    }

    private void createConfigFile() throws IOException {
        File file = new File("config.txt");
        if (!file.exists()) {
            System.out.println("Config file created: " + file.createNewFile());
            String content = "" +
                    "[databaseAddress] localhost\n" +
                    "[databaseName] zapsi2\n" +
                    "[userName] zapsi_uzivatel\n" +
                    "[userPassword] zapsi\n" +
                    "[customer] preciz\n" +
                    "[emailsFrom] support@zapsi.eu\n" +
                    "[port] 465\n" +
                    "[emailTo] zapsi@zapsi.eu\n" +
                    "[smtpHost] smtp.forpsi.com\n" +
                    "[smtpPassword] support01..\n" +
                    "[waitTime] 10";
            FileUtils.writeStringToFile(file, content, UTF_8);
        }
    }


    public void changePauseSettings(ActionEvent actionEvent) {
        pauseLogCheckBoxIsSelected = pauseButton.isSelected();
    }

    public void saveConfig(ActionEvent actionEvent) {
        saveDataToFile();

    }

    private void saveDataToFile() {
        try {
            File file = new File("config.txt");
            System.out.println("File created: " + file.createNewFile());
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
            BufferedWriter bw = new BufferedWriter(fw);
            String content = "" +
                    "[databaseAddress] " + databaseAddress.getText() +
                    "\n[databaseName] " + databaseName.getText() +
                    "\n[userName] " + databaseUsername.getText() +
                    "\n[userPassword] " + databasePassword.getText() +
                    "\n[deleteInterval] 10\n[customer] " + customer.getText() +
                    "\n[emailsFrom] " + emailsFrom.getText() +
                    "\n[port] " + port.getText() +
                    "\n[emailTo] " + emailsTo.getText() +
                    "\n[smtpHost] " + smtpHost.getText() +
                    "\n[smtpPassword] " + smtpPassword.getText();
            bw.write(content);
            bw.close();
            System.out.println("saved");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void changeServiceInstallStatus(ActionEvent actionEvent) {
        if (!serviceInstalled) {
            installService();
        } else if (!serviceRunning) {
            uninstallService();
        }
    }

    private void uninstallService() {
        try {
            while (serviceStatus.equals(ServiceStatus.STOPPED)) {
                Runtime.getRuntime().exec("sc delete ZapsiServer");
                checkForServiceStatus();
            }
            setServiceUninstalled();

        } catch (IOException e) {
            System.out.println("Missing service file.");
        }
    }

    private void installService() {
        try {
            File service = new File("ZapsiServerService.exe");
            String command = "sc create ZapsiServer binPath= \"" + service.getAbsolutePath() + "\" start= auto";
            while (serviceStatus.equals(ServiceStatus.NONE)) {
                Runtime.getRuntime().exec(command);
                checkForServiceStatus();
            }
            setServiceInstalled();
        } catch (IOException e) {
            System.out.println("Missing service file.");
        }
    }

    private void checkForServiceStatus() {
        try {
            String line;
            Process process = new ProcessBuilder("sc", "query", "ZapsiServer").start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder scOutput = new StringBuilder();
            while ((line = br.readLine()) != null) {
                scOutput.append(line).append("\n");
            }
            System.out.println(scOutput);
            updateServiceStatus(scOutput);
        } catch (IOException e) {
            System.out.println("No service present");
        }
    }

    private void updateServiceStatus(StringBuilder scOutput) {
        if (scOutput.toString().contains("RUNNING")) {
            serviceStatus = ServiceStatus.RUNNING;
            setServiceRunning();
        } else if (scOutput.toString().contains("STOPPED")) {
            serviceStatus = ServiceStatus.STOPPED;
            setServiceInstalled();
        } else if (scOutput.toString().contains("START_PENDING")) {
            serviceStatus = ServiceStatus.RUNNING;
            setServiceRunning();
        } else {
            setServiceUninstalled();
            serviceStatus = ServiceStatus.NONE;
        }
    }

    private void setServiceUninstalled() {
        serviceRunningStatus.setText("Service stopped");
        serviceRunningStatus.setStyle("-fx-fill: red");
        serviceInstallationStatus.setText("Service not installed");
        serviceInstallationStatus.setStyle("-fx-fill: red");
        installServiceButton.setText("Install");
        installServiceButton.setDisable(false);
        runServiceButton.setText("-");
        runServiceButton.setDisable(true);
        serviceInstalled = false;
        serviceRunning = false;
    }

    private void setServiceInstalled() {
        serviceRunningStatus.setText("Service stopped");
        serviceRunningStatus.setStyle("-fx-fill: red");
        serviceInstallationStatus.setText("Service installed");
        serviceInstallationStatus.setStyle("-fx-fill: green");
        installServiceButton.setText("Uninstall");
        installServiceButton.setDisable(false);
        runServiceButton.setText("Start");
        runServiceButton.setDisable(false);
        serviceInstalled = true;
        serviceRunning = false;
    }

    private void setServiceRunning() {
        serviceRunningStatus.setText("Service running");
        serviceRunningStatus.setStyle("-fx-fill: green");
        serviceInstallationStatus.setText("Service installed");
        serviceInstallationStatus.setStyle("-fx-fill: green");
        installServiceButton.setText("---");
        installServiceButton.setDisable(true);
        runServiceButton.setText("Stop");
        runServiceButton.setDisable(false);
        serviceInstalled = true;
        serviceRunning = true;
    }

    public void changeServiceRunStatus(ActionEvent actionEvent) throws InterruptedException {
        if (serviceRunning) {
            while (serviceStatus.equals(ServiceStatus.RUNNING)) {
                stopService();
                checkForServiceStatus();
            }
            setServiceInstalled();
        } else if (serviceInstalled) {
            while (serviceStatus.equals(ServiceStatus.STOPPED)) {
                runService();
                checkForServiceStatus();
            }
            setServiceRunning();
        }

    }

    private void runService() {
        try {
            Runtime rt = Runtime.getRuntime();
            rt.exec("sc start ZapsiServer");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopService() {
        try {
            Runtime rt = Runtime.getRuntime();
            rt.exec("sc stop ZapsiServer");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void devicesTabSelected(Event event) {
    }

    public void saveDevice(ActionEvent actionEvent) {
        int deviceOID = getDeviceOID(selectedDeviceRow);
        String name = deviceNameField.getText();
        String ipAddress = deviceIPField.getText();
        int isActive = 0;
        if (deviceIsActiveCheckBox.isSelected()) {
            isActive = 1;
        }
        try {
            String query = "UPDATE `zapsi2`.`device` SET `IPAddress`='" + ipAddress + "', `Name`='" + name + "', `Activated`=b'" + isActive + "' WHERE `OID`='" + deviceOID + "';\n";
            System.out.println(query);
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot update device");
        }
        devicesFromDatabase.clear();
        refreshDevicesTable();
    }

    public void addDevice(ActionEvent actionEvent) {
        int numberOfDevices = 0;
        try {
            String query = "SELECT count(oid) FROM zapsi2.device where devicetype=1";
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                numberOfDevices = resultSet.getInt("count(oid)");
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("cannot download devices from database");
        }
        try {
            numberOfDevices++;
            String query =
                    "INSERT INTO `zapsi2`.`device` (`CustomerID`, `HardwareID`, `FirmwareID`, `MacAddress`, `TypeName`, `IPAddress`, `Name`, `Activated`, `DownloadTimer`, `DeviceType`) " +
                            "VALUES ('1', '1', '1', '" + numberOfDevices + "', 'Zapsi', '192.168.0.1', 'New Device', b'0', '10', '1');";
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot insert state into database");
        }
        devicesFromDatabase.clear();
        refreshDevicesTable();
    }

    public void changePortDigital(ActionEvent actionEvent) {
        if (portIsDigital.isSelected()) {
            portIsAnalog.setSelected(false);
        }

    }

    public void changePortAnalog(ActionEvent actionEvent) {
        if (portIsAnalog.isSelected()) {
            portIsDigital.setSelected(false);
        }

    }

    public void addPort(ActionEvent actionEvent) {
        int deviceID = getDeviceOID(selectedDeviceRow);

        try {
            String query = "INSERT INTO `zapsi2`.`device_port` (`DeviceID`, `PortNumber`, `PortType`, `Name`, `Unit`, `Setting`) VALUES ('" + deviceID + "', '999', '999', 'Port Name', 'Unit', '' );\n";
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot insert state into database");
        }
        refreshPortsTable(selectedDeviceOID);

    }

    public void savePort(ActionEvent actionEvent) {
        int deviceOID = getDeviceOID(selectedDeviceRow);
        int portOID = getPortOID(selectedPortRow);
        int portNumber = Integer.parseInt(portNumberField.getText());
        String name = portNameField.getText();
        String portUnit = portUnitField.getText();
        String settings = portSettingsField.getText();
        int type = 2;
        if (portIsDigital.isSelected()) {
            type = 1;
        }

        try {
            String query = "UPDATE `zapsi2`.`device_port` SET `PortNumber`='" + portNumber + "', `PortType`='" + type + "', `Name`='" + name + "', `Unit`='" + portUnit + "', `Setting`='" + settings + "' WHERE `OID`='" + portOID + "';\n";
            System.out.println(query);
            PreparedStatement preparedStmt = (PreparedStatement) mySQLConnection.prepareStatement(query);
            preparedStmt.execute();
            preparedStmt.close();
        } catch (SQLException e) {
            System.out.println("Cannot update port");
        }
        refreshPortsTable(deviceOID);
    }
}
